# OpenML dataset: Linkedin_Job_Postings

https://www.openml.org/d/45952

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Description:
The "postings.csv" dataset comprises various job postings across different companies and locations. It includes detailed information on job titles, job descriptions, salaries, and application details. With columns specifying job IDs, company names, job titles, job descriptions, max salary, pay period, location, company IDs, views, median salary, minimum salary, formatted work type, number of applications, posting and listing times, remote work allowance, job posting URLs, application URLs, application types, expiry dates, closed times, experience levels, required skills, and work types. The dataset provides a rich source of information for analyzing job market trends, company hiring practices, and job seeker behaviors.

Attribute Description:
- job_id: Unique identifier for each job posting.
- company_name: Name of the company offering the job.
- title: Job title.
- description: Detailed job description.
- max_salary, med_salary, min_salary: Salary information (maximum, median, minimum).
- pay_period: Basis of salary compensation (e.g., hourly, yearly).
- location: Geographic location of the job.
- company_id: Unique identifier for each company.
- views: Number of views each posting has received.
- formatted_work_type: Employment type (e.g., full-time, part-time).
- applies: Number of applications submitted.
- original_listed_time, closed_time: Timestamps for when job postings were listed and closed.
- remote_allowed: Indicates if remote work is permitted.
- job_posting_url, application_url: URLs for the job posting and application submission.
- application_type: Method of application submission.
- expiry: Expiry date of the job posting.
- formatted_experience_level: Required experience level.
- skills_desc: Description of required skills.
- posting_domain: Domain of the job posting.
- sponsored: Indicates if posting is sponsored.
- work_type: Nature of the work (e.g., full-time, part-time).
- currency, compensation_type: Currency and type of compensation.

Use Case:
This dataset is invaluable for researchers and analysts focusing on labor market trends, HR professionals seeking comparative analysis for salary benchmarking or recruiting strategies, and job seekers looking to understand the dynamics of the job market. It can also be used to develop machine learning models to predict industry trends, salary ranges, and successful recruitment campaigns based on textual data from job descriptions and titles.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45952) of an [OpenML dataset](https://www.openml.org/d/45952). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45952/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45952/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45952/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

